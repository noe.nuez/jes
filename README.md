# JES PRUEBA TECNICA

### Clonar repositorio

```bash
$ git clone https://gitlab.com/noe.nuez/jes
```

## Frontend

### Variables de configuracion

```
# para cambiar la direccion de los servicios
frontend\src\app\http\index.ts
```

### Construir manualmente

```bash
# Ir a carpeta
$ cd front
# Instalar dependencias
$ npm install
# levanta en localhost:3000
$ npm start

```

### Construir docker local

```bash
# Ir a carpeta
$ cd front
# Contruir y levantar contenedor puesto 3001
$ docker-compose up -d --build

```
### Construir docker produccion

```bash
# Ir a carpeta
$ cd front
# Contruir y levantar contenedor puerto 1337
$ docker-compose -f docker-compose.prod.yml up -d --build
```
## Backend

### Variables 

```bash
# Variables de entorno
$ .env

```
### Construir

```bash
# Ir a carpeta
$ cd back
# levanta y construye los contenedores he imagenes puerto 1515
$ ./vendor/bin/sail up -d

```
