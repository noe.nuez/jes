<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablecimientoProjectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establecimiento_projections', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable(false);
            $table->string('adminNombre')->nullable(false);
            $table->string('direccion')->nullable(false);
            $table->string('latitud')->nullable(false);
            $table->string('longitud')->nullable(false);
            $table->string('telefono')->nullable(false);
            $table->bigInteger('numeroPedidosMes')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establecimiento_projections');
    }
}
