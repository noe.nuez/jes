<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('establecimiento/paginar', 'App\Http\Controllers\EstablecimientoController@paginar');
Route::resource('establecimiento', 'App\Http\Controllers\EstablecimientoController');
//Route::put("/establecimiento",'App\Http\Controllers\EstablecimientoController@update');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
