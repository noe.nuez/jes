<?php

namespace App\Models;

use App\Dominio\Events\EstablecimientoActualizado;
use App\Dominio\Events\EstablecimientoCreado;
use App\Dominio\Events\EstablecimientoEliminado;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Establecimiento extends Aggregate
{
    use HasFactory;
      protected $fillable = [
            'nombre',
            'adminNombre',
            'direccion',
            'latitud',
            'longitud',
            'telefono',
            'numeroPedidosMes'
        ];

    public function SetValues($nombre, $nombreAdmin, $direccion, $latitud, $longitud, $telefono, $totalPedidosMes)
    {
        $this->nombre=$nombre;
        $this->adminNombre =$nombreAdmin;
        $this->direccion=$direccion;
        $this->latitud=$latitud;
        $this->longitud=$longitud;
        $this->telefono=$telefono;
        $this->numeroPedidosMes=$totalPedidosMes;
        $this->apply(new EstablecimientoCreado($nombre, $nombreAdmin, $direccion, $latitud, $longitud, $telefono, $totalPedidosMes));
    }
    public  function  Actualizar($nombre, $nombreAdmin, $direccion, $latitud, $longitud, $telefono, $totalPedidosMes)
    {

        $this->nombre=$nombre;
        $this->adminNombre =$nombreAdmin;
        $this->direccion=$direccion;
        $this->latitud=$latitud;
        $this->longitud=$longitud;
        $this->telefono=$telefono;
        $this->numeroPedidosMes=$totalPedidosMes;
        $this->apply(new EstablecimientoActualizado($nombre, $nombreAdmin, $direccion, $latitud, $longitud, $telefono, $totalPedidosMes));

    }
    public function Eliminar(){
        $this->apply(new EstablecimientoEliminado($this->id));
    }
}
