<?php

namespace App\Models;

use App\Dominio\Events\IEvent;
use Illuminate\Database\Eloquent\Model;

abstract class Aggregate extends Model
{
    protected $eventos;
    public function __construct(array $attributes = [])
    {
        $this->eventos = collect([]);
        parent::__construct($attributes);

    }
    public function crear(array $attributes = []){
        return $this->create($attributes);
    }

    public function getRaisedEvents()
    {
        return $this->eventos;
    }
    protected function apply(IEvent $event)
    {
        $this->eventos->push($event);
    }
}
