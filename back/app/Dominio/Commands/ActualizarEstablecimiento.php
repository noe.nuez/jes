<?php

namespace App\Dominio\Commands;

use App\Bus\ICommand;

class ActualizarEstablecimiento implements ICommand
{
    public $nombre;
    public $adminNombre;
    public $direccion;
    public $latitud;
    public $longitud;
    public $telefono;
    public $numeroPedidosMes;
    public $id;

    public function __construct($id, $nombre, $nombreAdmin, $direccion, $latitud, $longitud, $telefono, $totalPedidosMes)
    {

        $this->nombre = $nombre;
        $this->adminNombre = $nombreAdmin;
        $this->direccion = $direccion;
        $this->latitud = $latitud;
        $this->longitud = $longitud;
        $this->telefono = $telefono;
        $this->numeroPedidosMes = $totalPedidosMes;
        $this->id = $id;
    }
}
