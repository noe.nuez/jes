<?php

namespace App\Dominio\Commands;

use App\Bus\ICommand;

class EliminarEstablecimiento implements ICommand
{

    public $id;

    public function __construct($id)
    {

        $this->id = $id;
    }
}
