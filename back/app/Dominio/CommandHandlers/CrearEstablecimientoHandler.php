<?php

namespace App\Dominio\CommandHandlers;

use App\Bus\ICommand;
use App\Bus\ICommandHandler;
use App\Models\Establecimiento;
use App\Repositories\IEstablecimientoRepository;

class CrearEstablecimientoHandler implements ICommandHandler
{
    /**
     * @var IEstablecimientoRepository
     */
    private $repo;

    public function __construct(IEstablecimientoRepository $repo)
    {
        $this->repo = $repo;
    }
    public function handle(ICommand $command)
    {
        $establecimiento=new Establecimiento();
        $establecimiento->SetValues($command->nombre,$command->adminNombre,$command->direccion,
            $command->latitud,$command->longitud,$command->telefono,$command->numeroPedidosMes);

        $this->repo->create($establecimiento);

    }


}
