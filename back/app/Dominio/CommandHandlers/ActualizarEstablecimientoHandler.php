<?php

namespace App\Dominio\CommandHandlers;

use App\Bus\ICommand;
use App\Bus\ICommandHandler;
use App\Models\Establecimiento;
use App\Repositories\IEstablecimientoRepository;
use League\Flysystem\Exception;

class ActualizarEstablecimientoHandler implements ICommandHandler
{
    /**
     * @var IEstablecimientoRepository
     */
    private $repo;

    public function __construct(IEstablecimientoRepository $repo)
    {
        $this->repo = $repo;
    }
    public function handle(ICommand $command)
    {
       $estabblecimiento=$this->repo->find($command->id);

        $estabblecimiento->actualizar($command->nombre,$command->adminNombre,$command->direccion,
           $command->latitud,$command->longitud,$command->telefono,$command->numeroPedidosMes);
        $this->repo->update($estabblecimiento, $command->id);

    }


}
