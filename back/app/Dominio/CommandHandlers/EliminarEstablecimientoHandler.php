<?php

namespace App\Dominio\CommandHandlers;

use App\Bus\ICommand;
use App\Bus\ICommandHandler;
use App\Models\Establecimiento;
use App\Repositories\IEstablecimientoRepository;
use League\Flysystem\Exception;

class EliminarEstablecimientoHandler implements ICommandHandler
{
    /**
     * @var IEstablecimientoRepository
     */
    private $repo;

    public function __construct(IEstablecimientoRepository $repo)
    {
        $this->repo = $repo;
    }
    public function handle(ICommand $command)
    {
       $estabblecimiento=$this->repo->find($command->id);

        $estabblecimiento->Eliminar();
        $this->repo->delete($estabblecimiento, $command->id);

    }


}
