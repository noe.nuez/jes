<?php

namespace App\Dominio\Proyections;

use App\Dominio\Events\IEvent;

interface IProjectorDispacher
{
    function Execute(IEvent $event, $entityId);
}
