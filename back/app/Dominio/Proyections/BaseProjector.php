<?php

namespace App\Dominio\Proyections;

use App\Dominio\Events\IEvent;

class BaseProjector
{
    protected $eventsToHandle = [];
    public function handleDomainEvent(IEvent $domainEvent, $entityId)
    {
        $eventName = class_basename($domainEvent);

        $this->{'when' . $eventName}($domainEvent, $entityId);

    }
    public function shouldHandleEvent(IEvent $domainEvent): bool {

        $collection = collect($this->eventsToHandle);
        return $collection->contains(get_class($domainEvent));
    }
}
