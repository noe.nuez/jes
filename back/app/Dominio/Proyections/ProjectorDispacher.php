<?php

namespace App\Dominio\Proyections;

use App\Dominio\Events\IEvent;

class ProjectorDispacher implements IProjectorDispacher
{
    private $events;

    public function __construct($events)
    {
        $this->events = $events;
    }

    function Execute(IEvent $event, $entityId)
    {
        foreach ($this->events as $valor) {

            if ($valor->shouldHandleEvent($event)) {
                return $valor->handleDomainEvent($event, $entityId);
            }
        }
    }
}
