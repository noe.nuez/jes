<?php

namespace App\Dominio\Proyections;

use App\Dominio\Events\EstablecimientoActualizado;
use App\Dominio\Events\EstablecimientoCreado;
use App\Dominio\Events\EstablecimientoEliminado;
use App\Dominio\Projectors\EstablecimientoProjection;
use App\Repositories\IEstablecimientoProjectionRepository;
use App\Repositories\IEstablecimientoRepository;
use League\Flysystem\Exception;

class EstablecimientoViewProyector extends BaseProjector
{
    private $establecimientoRepository;
    private $establecimientoProjectionRepository;

    public function __construct(IEstablecimientoRepository $establecimientoRepository, IEstablecimientoProjectionRepository  $establecimientoProjectionRepository)
    {
        $this->establecimientoRepository = $establecimientoRepository;
        $this->establecimientoProjectionRepository=$establecimientoProjectionRepository;
    }
    private function Create($entitiId){

        $entity=$this->establecimientoRepository->find($entitiId);

        $proyection=new EstablecimientoProjection();
        $this->getValues($entity, $proyection);
        $this->establecimientoProjectionRepository->create($proyection);
    }
    private  function Actualizar($entitiId){
        $entity=$this->establecimientoRepository->find($entitiId);
        $establecimiento= $this->establecimientoProjectionRepository->find($entitiId);

        $this->getValues($entity, $establecimiento);

        $this->establecimientoProjectionRepository->update($establecimiento, $entitiId);
    }
    protected $eventsToHandle = [
        EstablecimientoActualizado::class,
        EstablecimientoCreado::class,
        EstablecimientoEliminado::class
    ];

    public function whenEstablecimientoCreado(EstablecimientoCreado $creado, $entitiId){
        $this->Create($entitiId);
    }

    public function whenEstablecimientoActualizado(EstablecimientoActualizado $actualizado, $entitiId){
        $this->Actualizar($entitiId);
    }
    public function whenEstablecimientoEliminado(EstablecimientoEliminado $actualizado, $entitiId){
        $this->establecimientoProjectionRepository->delete($entitiId);
    }


    /**
     * @param $entity
     * @param $establecimiento
     */
    private function getValues($entity, $establecimiento): void
    {
        $establecimiento->id = $entity->id;
        $establecimiento->nombre = $entity->nombre;
        $establecimiento->adminNombre = $entity->adminNombre;
        $establecimiento->direccion = $entity->direccion;
        $establecimiento->latitud = $entity->latitud;
        $establecimiento->longitud = $entity->longitud;
        $establecimiento->telefono = $entity->telefono;
        $establecimiento->numeroPedidosMes = $entity->numeroPedidosMes;
    }
}
