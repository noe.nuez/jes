<?php

namespace App\Dominio\Queries;

Interface IQueryDispatcher
{
 function Execute(IQuery $query);
}
