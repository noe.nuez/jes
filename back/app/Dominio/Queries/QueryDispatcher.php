<?php

namespace App\Dominio\Queries;

use App\Bus\ICommandResolver;
use Illuminate\Container\Container;

class QueryDispatcher implements IQueryDispatcher
{
    private $queies;

    public function __construct($queies)
    {
        $this->queies = $queies;
    }
    function Execute(IQuery $query)
    {
        //return count($this->queies) ;
        foreach ($this->queies as $valor) {
            if($valor->PuedeEjecutar($query)){

                return $valor->Ejecutar($query);
            }
        }
    }
}
