<?php

namespace App\Dominio\Queries\Runners;

use App\Bus\ICommandBus;
use App\Dominio\Queries\ConsultarPaginado;
use App\Repositories\IEstablecimientoProjectionRepository;
use App\Repositories\IEstablecimientoRepository;

class ConsultarPaginadoRunner implements IQueryRunner
{

    private $repo;
    public function __construct(IEstablecimientoProjectionRepository $establecimientoRepository)
    {
        $this->repo=$establecimientoRepository;
    }
    function PuedeEjecutar($query)
    {
        return $query instanceof ConsultarPaginado;
    }

    function Ejecutar($query)
    {
        return $this->repo->paginate($query->page);
    }
}
