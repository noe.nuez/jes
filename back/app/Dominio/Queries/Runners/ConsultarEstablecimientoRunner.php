<?php

namespace App\Dominio\Queries\Runners;

use App\Bus\ICommandBus;
use App\Dominio\Queries\ConsultarEstablecimiento;
use App\Dominio\Queries\ConsultarPaginado;
use App\Dominio\Queries\IQuery;
use App\Repositories\IEstablecimientoProjectionRepository;
use App\Repositories\IEstablecimientoRepository;

class ConsultarEstablecimientoRunner implements IQueryRunner
{

    private $repo;
    public function __construct(IEstablecimientoProjectionRepository $establecimientoRepository)
    {
        $this->repo=$establecimientoRepository;
    }
    function PuedeEjecutar($query)
    {
        return $query instanceof ConsultarEstablecimiento;
    }

    function Ejecutar($query)
    {
        return $this->repo->find($query->establecimientoId);
    }
}
