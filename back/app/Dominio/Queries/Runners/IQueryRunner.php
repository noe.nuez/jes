<?php

namespace App\Dominio\Queries\Runners;

use App\Dominio\Queries\IQuery;

interface IQueryRunner
{
 function PuedeEjecutar(IQuery $query);
 function Ejecutar(IQuery $query);
}
