<?php

namespace App\Dominio\Queries;

use App\Dominio\EventHandler\IEventDispacher;
use App\Dominio\Proyections\IProjectorDispacher;
use App\Models\Establecimiento;

class ConsultarPaginado implements IQuery
{
    public $page;

    public function __construct($page)
    {
        $this->page = $page;
    }
}
