<?php

namespace App\Dominio\Queries;


class ConsultarEstablecimiento implements IQuery
{
    public $establecimientoId;

    public function __construct($establecimientoId)
    {
        $this->establecimientoId = $establecimientoId;
    }
}
