<?php

namespace App\Dominio\Events;

use App\Bus\ICommand;

class EstablecimientoActualizado implements IEvent
{
    public $nombre;
    public $adminNombre;
    public $direccion;
    public $latitud;
    public $longitud;
    public $telefono;
    public $numeroPedidosMes;

    public function __construct($nombre, $nombreAdmin, $direccion, $latitud, $longitud, $telefono, $totalPedidosMes)
    {

        $this->nombre = $nombre;
        $this->adminNombre = $nombreAdmin;
        $this->direccion = $direccion;
        $this->latitud = $latitud;
        $this->longitud = $longitud;
        $this->telefono = $telefono;
        $this->numeroPedidosMes = $totalPedidosMes;
    }
}
