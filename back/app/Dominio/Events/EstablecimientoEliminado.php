<?php

namespace App\Dominio\Events;

use App\Bus\ICommand;

class EstablecimientoEliminado implements IEvent
{

    private $idEstablecimiento;

    public function __construct($idEstablecimiento)
    {
        $this->idEstablecimiento = $idEstablecimiento;
    }
}
