<?php

namespace App\Dominio\Validators;

use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use PHPUnit\Framework\Constraint\Count;

class CrearEstablecimientoValidator implements  IValidator
{
 function validate($command){

     $validator = Validator::make((array)$command, [
         'nombre' => 'required|unique:establecimientos|max:255'
     ]);
    return $validator;

 }
}
