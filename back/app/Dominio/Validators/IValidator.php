<?php

namespace App\Dominio\Validators;

interface IValidator
{
    function validate($command);
}
