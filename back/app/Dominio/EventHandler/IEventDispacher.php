<?php

namespace App\Dominio\EventHandler;

use App\Dominio\Events\IEvent;
use App\Dominio\Queries\IQuery;

Interface IEventDispacher
{
    function Execute(IEvent $event, $entityId);
}
