<?php

namespace App\Dominio\EventHandler;

use App\Dominio\Events\IEvent;

interface IEventHandler
{
    function CanHandler(IEvent $event);
    function handle(IEvent  $event, $entityId);
}
