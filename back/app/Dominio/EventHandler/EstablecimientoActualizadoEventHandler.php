<?php

namespace App\Dominio\EventHandler;

use App\Dominio\EventHandler\IEventHandler;
use App\Dominio\Events\EstablecimientoActualizado;
use App\Dominio\Events\EstablecimientoCreado;
use App\Dominio\Events\IEvent;
use App\Repositories\IEstablecimientoRepository;
use League\Flysystem\Exception;

class EstablecimientoActualizadoEventHandler implements IEventHandler
{

    /**
     * @var IEstablecimientoRepository
     */
    private $establecimientoRepository;

    public function __construct(IEstablecimientoRepository $establecimientoRepository)
    {
        $this->establecimientoRepository = $establecimientoRepository;
    }

    function handle(IEvent $event, $entityId)
    {

    }

    function CanHandler(IEvent $event)
    {

       return $event instanceof EstablecimientoActualizado;
    }
}
