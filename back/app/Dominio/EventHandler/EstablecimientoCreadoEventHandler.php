<?php

namespace App\Dominio\EventHandler;

use App\Dominio\Events\EstablecimientoCreado;
use App\Dominio\Events\IEvent;
use App\Repositories\IEstablecimientoRepository;

class EstablecimientoCreadoEventHandler implements IEventHandler
{ /**
 * @var IEstablecimientoRepository
 */
    private $establecimientoRepository;

    public function __construct(IEstablecimientoRepository $establecimientoRepository)
    {
        $this->establecimientoRepository = $establecimientoRepository;
    }

    function handle(IEvent $event, $entityId)
    {
       // puedes aplicar la logica necesaria cuando lanza un evento
    }

    function CanHandler(IEvent $event)
    {

        return $event instanceof EstablecimientoCreado;
    }
}
