<?php

namespace App\Dominio\EventHandler;

use App\Dominio\Events\IEvent;
use App\Dominio\Queries\IQuery;
use League\Flysystem\Exception;

class EventDispacher implements IEventDispacher
{

    private $events;

    public function __construct($events)
    {
        $this->events = $events;
    }

    function Execute(IEvent $event, $entityId)
    {
        foreach ($this->events as $valor) {

            if ($valor->CanHandler($event)) {
                return $valor->handle($event, $entityId);
            }
        }
    }
}
