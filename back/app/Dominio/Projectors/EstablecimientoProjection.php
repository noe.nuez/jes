<?php


namespace App\Dominio\Projectors;

use App\Repositories\IEstablecimientoProjectionRepository;
use App\Repositories\IEstablecimientoRepository;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstablecimientoProjection extends Model
{

    use HasFactory;
    protected $fillable = [
        'id',
        'nombre',
        'adminNombre',
        'direccion',
        'latitud',
        'longitud',
        'telefono',
        'numeroPedidosMes'
    ];
}
