<?php

namespace App\Http\Controllers;

use App\Bus\ICommandBus;
use App\Dominio\Commands\ActualizarEstablecimiento;
use App\Dominio\Commands\CrearEstablecimiento;
use App\Dominio\Commands\EliminarEstablecimiento;
use App\Dominio\Commands\Test;
use App\Dominio\Queries\ConsultarEstablecimiento;
use App\Dominio\Queries\ConsultarPaginado;
use App\Dominio\Queries\IQueryDispatcher;
use App\Models\Establecimiento;

use App\Repositories\IEstablecimientoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class EstablecimientoController extends Controller
{

    private $commandBus;
    private $queryDispacher;
    public function __construct(ICommandBus $commandBus,IQueryDispatcher $queryDispatcher )
    {
        $this->commandBus = $commandBus;
        $this->queryDispacher=$queryDispatcher;
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $reques)
    {
        return  $this->queryDispacher->Execute(new ConsultarPaginado($reques->page));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       return  "post";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $valor=new CrearEstablecimiento($request->nombre,$request->adminNombre,$request->direccion,
            $request->latitud,$request->longitud,$request->telefono,$request->numeroPedidosMes);

        return $this->commandBus->execute($valor);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Establecimiento $establecimiento)
    {
       return $this->queryDispacher->Execute(new ConsultarEstablecimiento($establecimiento->id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Establecimiento $establecimiento)
    {
        return  "aqui";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $valor=new ActualizarEstablecimiento($id,$request->nombre,$request->adminNombre,$request->direccion,
            $request->latitud,$request->longitud,$request->telefono,$request->numeroPedidosMes);

        return $this->commandBus->execute($valor);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Establecimiento $establecimiento)
    {
       return $this->commandBus->execute(new EliminarEstablecimiento($establecimiento->id));
    }
}
