<?php
namespace App\Bus;
interface ICommandResolver
{
    function getCommandHandlerClass($command);
    function getCommandValidatorClass($command);
}
