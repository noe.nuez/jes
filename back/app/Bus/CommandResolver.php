<?php
namespace App\Bus;

class CommandResolver implements  ICommandResolver
{

    function getCommandHandlerClass($command)
    {
        return str_replace('Commands','CommandHandlers', get_class($command)) . 'Handler';
    }

    function getCommandValidatorClass($command)
    {
        return str_replace('Commands','Validators', get_class($command)) . 'Validator';
    }

}
