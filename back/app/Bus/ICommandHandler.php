<?php

namespace App\Bus;

interface ICommandHandler
{
    function handle(ICommand $command);
}
