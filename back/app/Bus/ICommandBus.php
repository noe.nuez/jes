<?php
namespace App\Bus;

interface ICommandBus
{
    function execute(ICommand $command);
}
