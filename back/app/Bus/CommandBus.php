<?php

namespace App\Bus;


use Illuminate\Auth\Events\Validated;
use Illuminate\Container\Container;
use League\Flysystem\Exception;

class CommandBus implements ICommandBus{

    private $container;

    private $resolver;

    public function __construct(Container $container, ICommandResolver $commandResolver)
    {
        $this->container = $container;
        $this->resolver = $commandResolver;
    }

    public function execute(ICommand $command)
    {
        $validator=$this->resolveValidator($command);
        $resul=$validator->validate($command);

        if(count($resul->errors())>0){
            throw new Exception($resul->getMessageBag());
        }
        return $this->resolveHandler($command)->handle($command);
    }

    public function resolveHandler($command)
    {
        $comandHandler = $this->resolver->getCommandHandlerClass($command);
        if(!class_exists($comandHandler))
            throw new Exception('comando no encontrado');

        $resolvedHandler = $this->container->make($comandHandler);

        return $resolvedHandler;
    }

    public function resolveValidator($command)
    {
        $comandHandler = $this->resolver->getCommandValidatorClass($command);
        if(!class_exists($comandHandler))
            throw new Exception('Validador no encontrado');

        $resolvedHandler = $this->container->make($comandHandler);

        return $resolvedHandler;
    }
}

