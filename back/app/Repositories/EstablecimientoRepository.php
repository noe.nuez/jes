<?php

namespace App\Repositories;

use App\Dominio\EventHandler\IEventDispacher;
use App\Dominio\Proyections\IProjectorDispacher;
use App\Models\Aggregate;
use App\Models\Establecimiento;

class EstablecimientoRepository implements IEstablecimientoRepository
{
    protected $model;
    /**
     * @var IEventDispacher
     */
    private $dispacher;
    /**
     * @var IProjectorDispacher
     */
    private $IProjectorDispacher;

    /**
     * PostRepository constructor.
     *
     * @param Post $post
     */
    public function __construct(Establecimiento  $post, IEventDispacher $dispacher, IProjectorDispacher $IProjectorDispacher)
    {
        $this->model = $post;
        $this->dispacher = $dispacher;
        $this->IProjectorDispacher = $IProjectorDispacher;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create(Aggregate $data)
    {
        $created=$data->crear(json_decode(json_encode($data), true));
        $this->RaiseEvents($data, $created->id);
        return $created;
    }

    public function update(Aggregate $data, $id)
    {
         $array = array(
          "nombre" => $data->nombre,
          'adminNombre'=>$data->adminNombre,
          'direccion'=>$data->direccion,
          'latitud'=>$data->latitud,
          'longitud'=>$data->longitud,
          'telefono'=>$data->telefono,
          'numeroPedidosMes'=>$data->numeroPedidosMes,
             'id'=>$id
      );
         $this->model->where('id', $id)->update($array);
        $this->RaiseEvents($data, $id);
    }

    public function delete($model)
    {
        $this->model->destroy($model->id);
        $this->RaiseEvents($model, $model->id);
    }

    public function find($id)
    {
        if (null == $post = $this->model->find($id)) {
            throw new ModelNotFoundException("establecimiento no encontrado");
        }
        return $post;
    }

    private function RaiseEvents(Aggregate $data, $entityId){
       $events=$data->getRaisedEvents();
        foreach ($events as $valor) {
           $this->dispacher->Execute($valor, $entityId);
           $this->IProjectorDispacher->Execute($valor, $entityId);
        }
    }
    public function paginate($page)
    {
        return $this->model->paginate(5,['*'],'page', $page);
    }
}
