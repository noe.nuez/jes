<?php

namespace App\Repositories;

use App\Models\Aggregate;

interface IRepository
{
    public function all();

    public function create(Aggregate  $data);

    public function update(Aggregate $data, $id);

    public function delete($id);

    public function find($id);
    public function paginate($page);
}
