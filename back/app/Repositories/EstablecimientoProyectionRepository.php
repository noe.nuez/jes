<?php

namespace App\Repositories;

use App\Dominio\EventHandler\IEventDispacher;
use App\Dominio\Projectors\EstablecimientoProjection;
use App\Models\Aggregate;
use App\Models\Establecimiento;
use League\Flysystem\Exception;

class EstablecimientoProyectionRepository implements IEstablecimientoProjectionRepository
{
    protected $model;
    /**
     * @var IEventDispacher
     */
    private $dispacher;

    /**
     * PostRepository constructor.
     *
     * @param Post $post
     */
    public function __construct(EstablecimientoProjection  $post)
    {
        $this->model = $post;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create($data)
    {

        $created=$this->model->create(json_decode(json_encode($data), true));
        return $created;
    }

    public function update($data, $id)
    {
        $array = array(
            "nombre" => $data->nombre,
            'adminNombre'=>$data->adminNombre,
            'direccion'=>$data->direccion,
            'latitud'=>$data->latitud,
            'longitud'=>$data->longitud,
            'telefono'=>$data->telefono,
            'numeroPedidosMes'=>$data->numeroPedidosMes,
            'id'=>$id
        );
        $this->model->where('id', $id)->update($array);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function find($id)
    {
        if (null == $post = $this->model->find($id)) {
            throw new ModelNotFoundException("establecimiento no encontrado");
        }
        return $post;
    }
    public function paginate($page)
    {

       return $this->model->paginate(5,['*'],'page', $page);
    }
}
