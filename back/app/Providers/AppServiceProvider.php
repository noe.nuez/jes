<?php

namespace App\Providers;

use App\Bus\CommandBus;
use App\Bus\CommandResolver;
use App\Bus\ICommandBus;
use App\Bus\ICommandResolver;
use App\Dominio\EventHandler\EstablecimientoActualizadoEventHandler;
use App\Dominio\EventHandler\EstablecimientoCreadoEventHandler;
use App\Dominio\EventHandler\EventDispacher;
use App\Dominio\EventHandler\IEventDispacher;
use App\Dominio\Proyections\EstablecimientoViewProyector;
use App\Dominio\Proyections\IProjectorDispacher;
use App\Dominio\Proyections\ProjectorDispacher;
use App\Dominio\Queries\IQueryDispatcher;
use App\Dominio\Queries\QueryDispatcher;
use App\Dominio\Queries\Runners\ConsultarEstablecimientoRunner;
use App\Dominio\Queries\Runners\ConsultarPaginadoRunner;
use App\Dominio\Queries\Runners\IQueryRunner;
use App\Repositories\EstablecimientoProyectionRepository;
use App\Repositories\EstablecimientoRepository;
use App\Repositories\IEstablecimientoProjectionRepository;
use App\Repositories\IEstablecimientoRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ICommandBus::class,CommandBus::class);
        $this->app->bind(ICommandResolver::class, CommandResolver::class);
        $this->app->bind(IEstablecimientoRepository::class, EstablecimientoRepository::class);
        $this->app->bind(IEstablecimientoProjectionRepository::class, EstablecimientoProyectionRepository::class);
        /*consultas*/

        $this->app->tag([ConsultarPaginadoRunner::class, ConsultarEstablecimientoRunner::class], 'queries');
        $this->app->bind(IQueryDispatcher::class, function ($app) {
            return new QueryDispatcher($app->tagged('queries'));
        });
        /***********/

        /*Eventos*/
        $this->app->tag([EstablecimientoCreadoEventHandler::class, EstablecimientoActualizadoEventHandler::class], 'events');
        $this->app->bind(IEventDispacher::class, function ($app) {
            return new EventDispacher($app->tagged('events'));
        });
        /***********/

        /*Proyections*/
        $this->app->tag([EstablecimientoViewProyector::class], 'projections');
        $this->app->bind(IProjectorDispacher::class, function ($app) {
            return new ProjectorDispacher($app->tagged('projections'));
        });
        /***********/

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
