

import { Grid } from 'app/components/form';
import React, { memo, useEffect, useState } from 'react';
import { Button, Card, Col, Row, Table } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import { base } from '../Sucursal/service';
interface Props {}

export const SucursalList = memo((props: Props) => {
  let history = useHistory();

  const colDefs = [
    {
      headerName: 'Acciones',
      type: 'actions',
      actions: [
        {
          label: 'Editar',
          style: 'btn-dark',
          action: row => {
            history.push('/sucursal/' + row.id);
          },
        },
      ],
    },
    {
      headerName: 'Nombre',
      field: 'nombre',
    },
    {
      headerName: 'Teléfono',
      field: 'telefono',
    },
    {
      headerName: 'Dirección',
      field: 'direccion',
    },
  ];
  return (
    <>
      <Card>
        <Card.Body>
          <Card.Title>Sucursales lista </Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            Lista de sucursales registradas
          </Card.Subtitle>
          <Row>
            <Col>
              <Button href="/sucursal" type="submit" variant="success">
                Nueva sucursal
              </Button>{' '}
            </Col>
          </Row>
          <hr></hr>
          <Row>
            <Col>
              <Grid colDefs={colDefs} url={base} pageSize={7}  />
            </Col>
          </Row>
        </Card.Body>
      </Card>{' '}
    </>
  );
});
