

import { lazyLoad } from 'utils/loadable';

export const SucursalList = lazyLoad(
  () => import('./index'),
  module => module.SucursalList,
);
