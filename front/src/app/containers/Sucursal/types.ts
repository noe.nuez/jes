import { GeoPosition } from "app/components/form/geo-marker/types";

/* --- STATE --- */
export interface SucursalState {
  form?:SucursalModel;
  redirect?:string;
}

export type ContainerState = SucursalState;

export interface SucursalModel {
  id?: number;
  nombre?: string;
  telefono?: string;
  direccion?: string;
  descripcion?: string;
  latitud?: number;
  longitud?: number;
  localizacion?:GeoPosition;
}