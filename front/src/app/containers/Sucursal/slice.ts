import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState, SucursalState } from './types';

export const initialState: ContainerState = {};

const SucursalSlice = createSlice({
  name: 'sucursal',
  initialState,
  reducers: {
    someAction(state, action: PayloadAction<any>) {},
    submitForm(state, action: PayloadAction<SucursalState>) {},
    eliminar(state, action: PayloadAction<SucursalState>) {},
    redirect(state, action: PayloadAction<SucursalState>) {
      state.redirect = action.payload.redirect;
    }
  },
});

export const {
  actions: sucursalActions,
  reducer,
  name: sliceKey,
} = SucursalSlice;
