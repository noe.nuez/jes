

import React, { memo, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as yup from 'yup';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey, sucursalActions as SucursalActions } from './slice';
import { selectSucursal } from './selectors';
import { sucursalSaga } from './saga';
import {
  GeoMarker,
  HookFormResolver,
  Text,
  TextArea,
} from 'app/components/form';
import { HookFormMethods } from 'app/components/form/hook-form/hookFormResolver';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { SucursalModel } from './types';
import { getSucursal } from './service';
import { useHistory, useParams } from 'react-router-dom';

interface Props { }
//const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
export const Sucursal = memo(() => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: sucursalSaga });
  const state = useSelector(selectSucursal);
  const history = useHistory();
  const [SucursalModel, setSucursalModel] = useState<SucursalModel>({});
  let { id } = useParams<any>();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  const onSubmit = (model: SucursalModel) => {
    model.id = id;
    model.latitud = model.localizacion?.lat;
    model.longitud = model.localizacion?.lng;
    model.localizacion = undefined;
    dispatch(SucursalActions.submitForm({ form: model }));
  };
  const onDelete = () => {
    dispatch(SucursalActions.eliminar({ form: { id: id } }));
  };
  const validations = yup.object().shape({
    nombre: yup.string().required('Nombre es requerido'),
    adminNombre: yup.string().required('nombre del administrador es requerida'),
    direccion: yup.string().required('Dirección es requerido'),
    telefono: yup
      .string()
      .required()
      .max(8),
      //.matches(phoneRegExp, 'Numero de telefono invalido'),
    localizacion: yup.object().nullable().required('Localización es requerido'),
    numeroPedidosMes:yup.string().test(
      'Solo numeros',
      'Solo numeros',
      function(value) {
        if(value?.length===0){return true;}
        var regex = /[0-9]|\./;
          if( !regex.test(value as string) ) {
            return false;
          }
        return true;
      }
    )
  });

  useEffect(() => {
    const fetchData = async () => {
      const result: any = await getSucursal(id);
      result.data.localizacion = {
        lat: result.data.latitud,
        lng: result.data.longitud,
      };
      setSucursalModel(result.data);
    };
    dispatch(SucursalActions.redirect({}));
    if (id) {
      fetchData();
    }
  }, []);
  if (id && !SucursalModel.nombre) {
    return <></>;
  }
  if (state.redirect) {
    history.push(state.redirect);
  }
  return (
    <>
      <HookFormResolver
        nameForm={'formGeneralData'}
        onSubmit={onSubmit}
        validations={validations}
        initialValues={SucursalModel}
      >
        {({ }: HookFormMethods) => {
          return (
            <>
              <Card>
                <Card.Body>
                  <Card.Title>Sucursales </Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">
                    Gestión de sucursales
                  </Card.Subtitle>
                  <Card.Body>
                    <Row>
                      <Col>
                        <Text name="nombre" label="Nombre de la sucursa" />
                      </Col>
                      <Col>
                        <Text name="telefono" label="Telefono" />
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Text name="adminNombre" label="Nombre del administrador" />
                      </Col>
                    </Row>
                    <Row>
                    <Col>
                        <Text name="numeroPedidosMes" label="Número de pedidos por mes" />
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <TextArea
                          rows={3}
                          label="Dirección completa"
                          name="direccion"
                        />
                      </Col>
                    </Row>
                    <hr></hr>
                    <GeoMarker
                      defaultValue={SucursalModel.localizacion}
                      label="Selecciona la ubicación"
                      name="localizacion"
                    />
                  </Card.Body>
                  <Button type="submit" variant="success">
                    {id ? 'Editar' : 'Crear'}
                  </Button>{' '}
                  
                  {id && (
                    <Button variant="danger" onClick={() => onDelete()}>
                      Eliminar
                    </Button>
                  )}
                  {' '}
                  <Button variant="secondary" href="/sucursal-lista">
                    Cancelar
                  </Button>
                </Card.Body>
              </Card>{' '}
            </>
          );
        }}
      </HookFormResolver>
    </>
  );
});
