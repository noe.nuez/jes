import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.sucursal || initialState;

export const selectSucursal = createSelector(
  [selectDomain],
  sucursalState => sucursalState,
);
