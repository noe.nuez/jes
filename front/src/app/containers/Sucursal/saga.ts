import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, takeLatest } from 'redux-saga/effects';
import { sucursalActions } from './slice';
import { SucursalState } from './types';
import { actualizar, crear, eliminar } from './service';
import toast, { Toaster } from 'react-hot-toast';
export function* submitFormHandler(action: PayloadAction<SucursalState>) {
  var model = action.payload.form;
  try {
    if (model?.id) {
      yield call(actualizar, model);
    } else {
      yield call(crear, model);
    }
    yield put(sucursalActions.redirect({ redirect: '/sucursal-lista' }));
  } catch (err) {
    toast.error(err.response.data.message)
  
  }
}
export function* EliminarHandler(action: PayloadAction<SucursalState>) {
  try {
    yield call(eliminar, action.payload.form?.id);
    yield put(sucursalActions.redirect({ redirect: '/sucursal-lista' }));
  } catch (err) {
    toast.error(err.response.data.message)
  }
}
export function* sucursalSaga() {
  yield takeLatest(sucursalActions.submitForm.type, submitFormHandler);
  yield takeLatest(sucursalActions.eliminar.type, EliminarHandler);
}
