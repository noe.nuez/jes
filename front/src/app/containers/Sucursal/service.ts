import http from '../../http/index';
import { SucursalModel } from './types';
export const base = '/api/establecimiento';

export const getSucursal = (id: number) => {
  return http.get(base + '/' + id);
};
export const crear = (establecimiento?: SucursalModel) => {
  return http.post(base, establecimiento);
};

export const actualizar = (establecimiento?: SucursalModel) => {
  return http.put(base + '/' + establecimiento?.id, establecimiento);
};
export const eliminar = (id?: number) => {
  return http.delete(base + '/' + id);
};
