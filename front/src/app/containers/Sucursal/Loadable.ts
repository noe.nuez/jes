

import { lazyLoad } from 'utils/loadable';

export const Sucursal = lazyLoad(
  () => import('./index'),
  module => module.Sucursal,
);
