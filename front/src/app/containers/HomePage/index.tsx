import * as React from 'react';
import { Card, Row, Col, Form, Alert } from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';

export function HomePage() {
  return (
    <>
      <Card>
        <Card.Body>
          <Card.Title>Información </Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            Prueba tecnica
          </Card.Subtitle>
          <Row>
            <Col>
              <Form.Label>Noe Saul Nuñez</Form.Label>
            </Col>
          </Row>
          <Row>
            <Col>
              <Alert variant="secondary">
                <Alert.Heading>Enunciado</Alert.Heading>
                <p>
                La empresa XYZ necesita un herramienta informática para llevar el registro de sus sucursales para lo cual
necesita que se resuelvan las siguientes funcionalidades.
                </p>
                <hr />
              </Alert>
            </Col>
          </Row>
          <hr></hr>
          <Row>
            <Col>
              <Alert variant="secondary">
                <Alert.Heading>Componentes</Alert.Heading>
                <ul>
                  <li>Fronteend react/typescript</li>
                  <li>Backend levantado con Laravel</li>
                  <li>Creación, inserción y eliminación de sucursales</li>
                </ul>
              </Alert>
            </Col>
          </Row>
        </Card.Body>
      </Card>{' '}
    </>
  );
}
